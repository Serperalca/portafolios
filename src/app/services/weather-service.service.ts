import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe } from 'rxjs';
import { rejects } from 'assert';

@Injectable({
  providedIn: 'root'
})
export class WeatherServiceService {
  
  constructor( public http: HttpClient) { }

    daysWeather = ()=> {
      return fetch("https://api.openweathermap.org/data/2.5/forecast?q=Madrid,es&units=metric&appid=b9b020b6d1e6f27648dabb6230f7e5cc")
      .then(data => data.json ()) 
    }
  /**
   * 
   * @description Funcion llamada Rest a API.
   * @author Sergio
   * @callback Observable
   * @argument string pelicula
   */  
    getPelicula = (pelicula) => fetch("https://www.omdbapi.com/?t="+pelicula+"&apikey=2a0b18").then(data => data.json());

    // getEventos() : Observable<any>{
    //   return this.http.get("https://datos.madrid.es/egob/catalogo/212504-0-agenda-actividades-deportes.json");
    // }

    getEventos = () => fetch ("https://datos.madrid.es/egob/catalogo/212504-0-agenda-actividades-deportes.json");
}
