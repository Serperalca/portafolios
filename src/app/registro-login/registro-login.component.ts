import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registro-login',
  templateUrl: './registro-login.component.html',
  styleUrls: ['./registro-login.component.scss']
})
export class RegistroLoginComponent implements OnInit {
  email: string;
  password: string;
  confirmPassword: string;

  constructor( private fb: FormBuilder ) { }

  ngOnInit(): void {
  }

  register() {
    console.log(this.email);
    console.log(this.password);
  }
}
