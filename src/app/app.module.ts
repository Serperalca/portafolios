// Módulos
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';

//Material Modules
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';

//servicios
import { WeatherServiceService } from '../app/services/weather-service.service';
import { CookieService } from 'ngx-cookie-service';
import { UsersService} from '../app/services/users.service';

//componentes
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { GraficoComponent } from './grafico/grafico.component';
import { MisProyectosComponent } from './mis-proyectos/mis-proyectos.component';
import { ContactoComponent } from './contacto/contacto.component';
import { MenuTemporalComponent } from './menu-temporal/menu-temporal.component';
import { BuscapelisComponent } from './buscapelis/buscapelis.component';
import { RegistroLoginComponent } from './registro-login/registro-login.component';
import { MadriDeporteComponent } from './madri-deporte/madri-deporte.component';
import { CVComponent } from './cv/cv.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    GraficoComponent,
    ContactoComponent,
    MisProyectosComponent,
    MenuTemporalComponent,
    BuscapelisComponent,
    RegistroLoginComponent,
    MadriDeporteComponent,
    CVComponent,
   
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatMenuModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatGridListModule,
    MatSidenavModule,
    MatListModule,
    MatExpansionModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
    
  ],
  providers: 
  [
    WeatherServiceService, 
    UsersService, 
    CookieService 
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
