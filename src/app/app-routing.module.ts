import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { GraficoComponent } from '../app/grafico/grafico.component';
// import { MenuComponent } from '../app/menu/menu.component';
import { ContactoComponent } from '../app/contacto/contacto.component';
import { MenuTemporalComponent } from './menu-temporal/menu-temporal.component';
import { MisProyectosComponent } from './mis-proyectos/mis-proyectos.component';
import { CVComponent } from './cv/cv.component';
// import { PruebaComponent } from './prueba/prueba.component';

const routes: Routes = [
  {path: '', component: MisProyectosComponent },
  {path: 'cv', component: CVComponent },
  // {path: 'grafico', component: GraficoComponent},
  {path: 'contacto', component: ContactoComponent},
  {path: 'misproyectos', component: MisProyectosComponent},
  
  {path: 'prueba', component: MenuTemporalComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
