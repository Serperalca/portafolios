import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mis-proyectos',
  templateUrl: './mis-proyectos.component.html',
  styleUrls: ['./mis-proyectos.component.scss']
})
export class MisProyectosComponent implements OnInit {
  mostrar: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  desvelar(){
    this.mostrar = (!this.mostrar);
  }

}
