import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { WeatherServiceService } from '../services/weather-service.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-grafico',
  templateUrl: './grafico.component.html',
  styleUrls: ['./grafico.component.scss']
})
export class GraficoComponent implements OnInit {
  
  mostrar: boolean = false;
  tempMax: number[] = [];
  tempMin: number[] = [];
  days: string[] = [];
  clima: string[];
  
  constructor(
    public wS:WeatherServiceService,
    private route: ActivatedRoute,   //ruta concreta
    private router: Router           // enrutado completo
  ) { }
  ngOnInit(): void {
  }

  async getClima(){
    // this.mostrar = true;
    this.ocultar();
    const res= await this.wS.daysWeather(); //De esta forma convierto en síncrona esta función.
      let i: number = 0;
      for( i; i < res.list.length; i++ ){ 
        this.tempMax.push(res.list[i].main.temp_max);
        this.tempMin.push(res.list[i].main.temp_min);
        this.days.push(res.list[i].dt_txt);
      }
      this.mostrarGrafico();
      

  }

  mostrarGrafico = () => {
    console.log("cargando grafico")
    console.log(this.tempMax, this.tempMin);
    var chart = new Chart('canvas', {
      type: 'line',
      data: {
          labels: this.days,
          datasets: [
            {
              label: "Temperatura Máxima",
              data: this.tempMax,
              backgroundColor: "green",
              fill: false,
              borderWidth: 1
            },
            {
              data: this.tempMin,
              backgroundColor: "aqua",
              fill: false,
              borderWidth: 1,

              type: 'bar'
            }  
        ], 
      },
              options: {
                legend: {
                  display: false
                },
                  scales: {
                    xAxes: [
                      {
                        display: true
                      }
                    ],
                      yAxes: [{
                        display: true,
                          ticks: {
                              beginAtZero: true
                          }
                      }]
                  }
              },
          });
         
          chart.update();
          
    }    
    
    ocultar(){
      this.mostrar = (!this.mostrar);
    }
 
}
