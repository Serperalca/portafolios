import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscapelisComponent } from './buscapelis.component';

describe('BuscapelisComponent', () => {
  let component: BuscapelisComponent;
  let fixture: ComponentFixture<BuscapelisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuscapelisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscapelisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
