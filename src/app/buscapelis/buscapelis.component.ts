import { Component, OnInit } from '@angular/core';
import { WeatherServiceService } from '../services/weather-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-buscapelis',
  templateUrl: './buscapelis.component.html',
  styleUrls: ['./buscapelis.component.scss']
})

export class BuscapelisComponent implements OnInit {

  mostrar: boolean = false;
  portada : any;
  data : any;
  movie : string;
  // res: any;
  peli: any;
  titulo: string = '';
  actores : string = '';
  director: string = '';
  genero : string = '';
  

  constructor( 
    public wS : WeatherServiceService,
    private fb: FormBuilder
    ) { }

  ngOnInit(): void {
  }

  async buscar(){
    this.ocultar();
    let pelicula = encodeURIComponent(this.movie);
    const respuesta= await this.wS.getPelicula(pelicula)
     console.log(respuesta);
     this.titulo = respuesta.Title;
     this.portada = respuesta.Poster;
     this.actores = respuesta.Actors;
     this.director= respuesta.Director;
     this.genero = respuesta.Genre;
 
  }

  ocultar(){
    this.mostrar = (!this.mostrar);
  }
  /**
   * @description Función para obtener respuesta de Api 
   * y mostrarla a través de un observable.
   * Me falla al obtener las propiedades del objeto de respuesta.
   */
  // buscar(){
  //   let pelicula = encodeURIComponent(this.movie);
  //   this.wS.getPelicula(pelicula).subscribe(data=>{
  //     console.log(data);
  //     this.titulo = data.Title;
  //   });
  // }

}

  

