import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MadriDeporteComponent } from './madri-deporte.component';

describe('MadriDeporteComponent', () => {
  let component: MadriDeporteComponent;
  let fixture: ComponentFixture<MadriDeporteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MadriDeporteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MadriDeporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
