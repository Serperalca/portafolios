import { Component, OnInit } from '@angular/core';
import { WeatherServiceService } from '../services/weather-service.service';

@Component({
  selector: 'app-madri-deporte',
  templateUrl: './madri-deporte.component.html',
  styleUrls: ['./madri-deporte.component.scss']
})
export class MadriDeporteComponent implements OnInit {

  mostrar : boolean = false;
  evento = [];
  fechaInicio = [];
  fechaFin = [];
  precio = [];
  enlace = [];
  
  constructor( private wS : WeatherServiceService ) { }

    ngOnInit(): void {
      // let i: number;
      // this.wS.getEventos().subscribe(data =>{
      //   console.log(data);
      // for (i = 0; i < data.length; i++){
      //   this.evento = [data.title]; 
      //   this.fechaInicio = [data.dtstart];
      //   this.fechaFin = [data.dtend];
      //   this.precio = [data.precio];
      //   this.enlace = [data.enlace];
      // } 
      
    // },
    //   error =>{
    //     console.error(error);
    //     console.log("ERROR EN LA COMUNICACION")
    //   });
    }

  async desvelar(){
    this.muestra();
    const response = await this.wS.getEventos();
    console.log(response);
  }

  muestra(){
    this.mostrar = (!this.mostrar);
  }

}
