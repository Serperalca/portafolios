import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuTemporalComponent } from './menu-temporal.component';

describe('MenuTemporalComponent', () => {
  let component: MenuTemporalComponent;
  let fixture: ComponentFixture<MenuTemporalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuTemporalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuTemporalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
