import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-temporal',
  templateUrl: './menu-temporal.component.html',
  styleUrls: ['./menu-temporal.component.scss']
})
export class MenuTemporalComponent implements OnInit {

  constructor( private router: Router ) { }

  ngOnInit(): void {
  }

  direccionar(url:string){
    this.router.navigate([url])
  }
}
